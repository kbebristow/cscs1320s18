#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <string>
#include <fstream>

using namespace std;

int main( ) { 
    int integer;
    double real;
    string word;
		bool b = false;
		int neg = -99;
		string ws = "whitespace";
		double f = 80.1234567;
	
    cout << "enter an integer: ";
    cin >> integer;
    cout << "integer: " << integer << endl;
	
    cout << "enter a real number that has a decimal fraction: ";
    cin >> real;
    cout << "real: " << real << endl;

    cout << "enter a word: ";
    cin >> word;
    cout << "word: " << word << endl;
	
	
	cout << "boolalpha: " << boolalpha << b << endl;
	cout << "boolalpha: " << noboolalpha << b << endl;
	cout << "dec: " << dec << real << endl;
	cout << "fixed: " << fixed << real << endl;
	cout << "hex: " << hex << integer << endl;
	cout << "internal: " << dec << internal << integer << endl;
	cout << "left: " << setfill ( 'x' ) <<  setw(10) << left << word << endl;
	cout << "oct: " << integer << endl;
	cout << "right: " << setfill ( 'x' ) << setw(10) << right << word << endl;
	cout << "scientific: " << noshowpos << scientific << setprecision(3) << f << real << endl;
	cout << "showbase " << hex << showbase << integer << endl;
	cout << "showpoint: " << dec << showpoint << integer << endl;
	cout << "showpos: " << dec << showpos << real << '\t' << integer << '\t' << neg << endl;
	cout << "skipws: " << '\t' << ws << endl;
	cout << "uppercase: " << hex << uppercase << integer << endl;
	
    return 0;
}